DIR_PROTEUS    = proteus
DIR_TOOLCHAIN  = toolchain
DIR_EVALUATION = evaluation

GIT    = git
APT    = apt
PYTHON = python3

APTS  =
APTS += cmake
APTS += ninja-build
APTS += g++
APTS += curl
APTS += openjdk-17-jdk
APTS += verilator
APTS += python3-pip
APTS += autoconf
APTS += automake
APTS += autotools-dev
APTS += libmpc-dev
APTS += libmpfr-dev
APTS += libgmp-dev
APTS += gawk
APTS += build-essential
APTS += bison
APTS += flex
APTS += texinfo
APTS += gperf
APTS += libtool
APTS += patchutils
APTS += bc
APTS += zlib1g-dev
APTS += libexpat-dev
APTS += ninja-build
APTS += cmake
APTS += libglib2.0-dev

PIPS  =
PIPS += vcdvcd
PIPS += matplotlib
PIPS += scipy

.PHONY: status
status:
	$(GIT) status -sb
	$(GIT) -C $(DIR_PROTEUS)    status -sb
	$(GIT) -C $(DIR_TOOLCHAIN)  status -sb
	$(GIT) -C $(DIR_EVALUATION) status -sb

.PHONY: install-deps
install-deps:
	sudo $(APT) install $(APTS)
	$(PYTHON) -m pip install --user $(PIPS)
	$(MAKE) install-riscv-gnu-toolchain
	$(MAKE) install-sbt

.PHONY: install-riscv-gnu-toolchain
install-riscv-gnu-toolchain:
	$(GIT) clone --depth=1 --branch 2023.07.07 https://github.com/riscv/riscv-gnu-toolchain
	cd riscv-gnu-toolchain && ./configure --prefix=/opt/riscv --with-arch=rv32im_zicsr
	cd riscv-gnu-toolchain && sudo make

.PHONY: install-sbt
install-sbt:
	echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | sudo tee /etc/apt/sources.list.d/sbt.list
	echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | sudo tee /etc/apt/sources.list.d/sbt_old.list
	curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | sudo apt-key add
	sudo apt-get update
	sudo apt-get install sbt

.PHONY: build-proteus-static
build-proteus-static:
	$(GIT) -C $(DIR_EVALUATION) checkout main
	$(RM) -rf $(DIR_PROTEUS)/tests/build
	$(RM) -rf $(DIR_PROTEUS)/sim/build
	$(MAKE) -C $(DIR_PROTEUS)/tests

.PHONY: build-proteus-dynamic
build-proteus-dynamic:
	$(GIT) -C $(DIR_EVALUATION) checkout dynamic
	$(RM) -rf $(DIR_PROTEUS)/tests/build
	$(RM) -rf $(DIR_PROTEUS)/sim/build
	$(MAKE) -C $(DIR_PROTEUS)/tests CORE=riscv.CoreDynamicExtMem

.PHONY: build-toolchain
build-toolchain:
	$(MAKE) -C $(DIR_TOOLCHAIN) configure-build
	$(MAKE) -C $(DIR_TOOLCHAIN) build
	$(MAKE) -C $(DIR_TOOLCHAIN) install

.PHONY: evaluate
evaluate:
	$(MAKE) -C $(DIR_EVALUATION) realclean
	$(MAKE) -C $(DIR_EVALUATION)
