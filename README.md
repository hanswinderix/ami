# Architectural Mimicry: Innovative Instructions to Efficiently Address Control-Flow Leakage in Data-Oblivious Programs

Architectural Mimicry (AMi) is a hardware-software co-designed countermeasure to prevent leaking secrets via a program's control flow. For more information about AMi, we refer to our [S&P 2024 paper](https://mici.hu/papers/winderix24ami.pdf).

```
@inproceedings{winderix:2024-ami,
  author    = {Winderix, Hans and Bognar, Marton and Noorman, Job and Daniel, Lesly-Ann and Piessens, Frank},
  booktitle = {2024 IEEE Symposium on Security and Privacy (SP)},
  title     = {Architectural Mimicry: Innovative Instructions to Efficiently Address Control-Flow Leakage in Data-Oblivious Programs},
  year      = {2024}
}
```

This repository contains the source code of the RISC-V hardware, of the RISC-V toolchain, and of the full set of benchmarks and experiments required to perform the security and the performance evaluation as presented in the [paper](https://mici.hu/papers/winderix24ami.pdf). In this document, we provide the needed information to reproduce the main results on a fresh 64-bit Ubuntu 22.04 LTS installation.

## System requirements

- **OS**: Fresh Ubuntu 22.04 (64 bit)
- **RAM**: 8 GB
- **Free Disk space**: 10 GB

## Getting the source code

1. Install the following dependencies.

   * GNU **make** 4.3 or later
   * **Git**

   ```sh
   sudo apt install make git
   ```

   Please note that since `apt` installs software system-wide (for every user), you need to have superuser privileges to be able to run this command. Since `sudo` is used to temporarily acquire these privileges, you will be asked to provide your password.

2. Clone the Git repository.

   ```sh
   git clone --recursive https://gitlab.com/hanswinderix/ami.git
   ```

3. Change the directory to the cloned repository

   ```sh
   cd ami
   ```

## Installing additional dependencies

```sh
make install-deps
```

Please note that since the `install-deps` `make` target installs software system-wide (for every user), you need to have superuser privileges to be able to run this command. Since under the hood `sudo` is used (twice) to temporarily acquire these privileges, you will be asked to provide your password (twice).

## Building the AMi toolchain

```sh
make build-toolchain
```

## Running the AMi evaluation (in-order pipeline)

To run the correctness, security and performance evaluations of our AMi implemenation for the in-order pipeline:

```sh
make build-proteus-static
make evaluate
```

The performance evaluation results (Table 5 in the [paper](https://mici.hu/papers/winderix24ami.pdf)) are saved in the csv file evaluation/report.csv.

## Running the AMi evaluation (out-of-order pipeline)

To run the correctness, security and performance evaluations of our AMi implemenation for the out-of-order pipeline:

```sh
make build-proteus-dynamic
make evaluate
```

The performance evaluation results (Table 6 in the [paper](https://mici.hu/papers/winderix24ami.pdf)) are saved in the csv file evaluation/report.csv.

## Hardware cost measurements

These measurements are conducted using [Xilinx Vivado](https://www.xilinx.com/products/design-tools/vivado/vivado-ml.html), which requires approximately 52 GB of disk space.

First, run the evaluation for the desired pipeline (in-order or out-of-order) as indicated above. This will create a file `proteus/Core.v` which will be used for the hardware cost measurement.

### Installation instructions

1. Download and run the the installer for [Vivado](https://www.xilinx.com/support/download.html) (requires a free account).
2. Select Product to Install: Vivado (ML Standard).
3. Customize the installation: Vivado Design Suite and the 7 Series Production Devices are sufficient, everything else can be deselected.

### Creating a project

1. Launch Vivado, and start the Create Project wizard.
2. Choose the project name and location as desired.
3. Project type: RTL Project.
4. Add sources: select `proteus/Core.v` (created while running the evaluation) and `proteus/synthesis/Top.v`. Do **not** check "Copy sources into project" or "Scan and add RTL include files into project".
5. Add constraints: select `proteus/synthesis/Constraints.xdc`.
6. Default part: select your target FPGA, the `xc7a35ticsg324-1L`.
7. Finish the wizard.
8. When the project is open, if `Top.v` is not selected as the top module (shown in bold), right-click on it and "Set as Top".

### Running the cost measurement

9. Change the timing constraint for the critical path in `Constraints.xdc` to the value indicated in the paper (e.g., `17.1 ns` for the in-order pipeline). As the synthesis process is not completely deterministic, you might need to increase this value slightly if the implementation fails with a failed timing. For example, to change the timing constraint to `17.1 ns`, replace `-period 31.000` in `Constraints.xdc` with `-period 17.100`.
10. Click "Run Implementation" (which will run synthesis first if necessary).

When the implementation finishes, the relevant values for number of LUTs and registers can be found on the "Design Runs" tab at the bottom of the screen, in line "impl_1" and in the columns "LUT" and "FF", respectively.
If the timing constraint for the critical path is met, this is shown by a black (and positive) number under the WNS (worst negative slack) field in the same table, failed timings are indicated by an error and a red (negative) WNS number.

To obtain the baseline hardware cost measurements, check out the [base Proteus repository](https://github.com/proteus-core/proteus) at tag `v23.03`, run `make -C sim` for the in-order pipeline or `make -C sim CORE=riscv.CoreDynamicExtMem` for the out-of-order pipeline and overwrite `proteus/Core.v` in this repository with the generated `Core.v`, then proceed with instructions 9-10 as before.
